package daug.dependencies.injections.dioldway

import android.app.Application
import daug.dependencies.injections.dioldway.component.ApplicationComponent
import daug.dependencies.injections.dioldway.component.DaggerApplicationComponent
import daug.dependencies.injections.dioldway.module.ApplicationModule

/**
 * Created by Hospice on 20/03/2018.
 */

class MyApplication : Application() {

    companion object {
        lateinit var mApplicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))

                .build()

        mApplicationComponent.inject(this)
    }

}

