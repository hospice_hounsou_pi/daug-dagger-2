package daug.dependencies.injections.dioldway.component

import dagger.Component
import daug.dependencies.injections.dioldway.MainActivity
import daug.dependencies.injections.dioldway.module.ActivityModule
import daug.dependencies.injections.dioldway.scope.PerActivity

/**
 * Created by Hospice on 20/03/2018.
 */
@PerActivity
@Component(dependencies = [(ApplicationComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}