package daug.dependencies.injections.dioldway.module

import android.content.Context
import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import daug.dependencies.injections.dioldway.scope.ActivityContext
import javax.inject.Singleton

/**
 * Created by Hospice on 20/03/2018.
 */
@Module
class ActivityModule(private var  mActivity : AppCompatActivity) {

    @Provides
    @Singleton
    @ActivityContext
    fun provideContext(): Context {
        return mActivity
    }
}