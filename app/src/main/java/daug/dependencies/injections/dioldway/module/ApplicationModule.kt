package daug.dependencies.injections.dioldway.module

import android.content.Context
import dagger.Module
import dagger.Provides
import daug.dependencies.injections.dioldway.MyApplication
import daug.dependencies.injections.minterface.EmailService
import daug.dependencies.injections.minterface.MessageService
import daug.dependencies.injections.dioldway.scope.ApplicationContext
import javax.inject.Singleton

/**
 * Created by Hospice on 20/03/2018.
 */

@Module
class ApplicationModule(private var  mApplication : MyApplication) {



    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(): Context {
        return mApplication
    }


    @Provides
    @Singleton
    fun provideMessageService(): MessageService {
        return EmailService()
    }

}