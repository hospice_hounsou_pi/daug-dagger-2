package daug.dependencies.injections.dioldway

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import daug.dependencies.injections.R
import daug.dependencies.injections.dioldway.component.ActivityComponent
import daug.dependencies.injections.dioldway.component.DaggerActivityComponent
import daug.dependencies.injections.data.Person
import daug.dependencies.injections.dioldway.module.ActivityModule
import javax.inject.Inject

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    lateinit var mActivityComponent: ActivityComponent
    @Inject
    lateinit var mPerson : Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .applicationComponent(MyApplication.Companion.mApplicationComponent)
                .build()
        mActivityComponent.inject(this)


       // mPerson.greetFriend()

    }
}
