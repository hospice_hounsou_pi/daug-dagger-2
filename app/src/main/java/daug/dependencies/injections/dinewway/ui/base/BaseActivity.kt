package daug.dependencies.injections.dinewway.ui.base

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dagger.android.AndroidInjection

open class BaseActivity : AppCompatActivity() , MvpView{

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun onError(resId: Int) {
     }

    override fun onError(message: String) {
    }

    override fun showMessage(message: String) {
     }

    override fun showMessage(resId: Int) {
    }


}
