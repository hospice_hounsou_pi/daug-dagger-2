package daug.dependencies.injections.dinewway.ui.base

import daug.dependencies.injections.dinewway.ui.MainActivityContrat

/**
 * Created by Hospice on 22/03/2018.
 */

open class BasePresenter<V : MainActivityContrat.View> : MvpPresenter<V> {
    lateinit var mMvpView: V

    override fun onAttach(mvpView: V) {
        mMvpView = mvpView
    }

    override fun onDetach() {

    }
}
