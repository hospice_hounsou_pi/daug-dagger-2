package daug.dependencies.injections.dinewway.component

import android.app.Application

import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import daug.dependencies.injections.dinewway.MyApplication
import daug.dependencies.injections.dinewway.activitybuilder.ActivityBuilder
import daug.dependencies.injections.dinewway.module.ApplicationModule

/**
 * Created by Hospice on 20/03/2018.
 */

@Singleton
@Component(modules = [AndroidInjectionModule::class, ApplicationModule::class, ActivityBuilder::class])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent

    }

    fun inject(app: MyApplication)

}
