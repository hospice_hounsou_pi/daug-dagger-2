package daug.dependencies.injections.dinewway

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.DispatchingAndroidInjector
import daug.dependencies.injections.dinewway.component.DaggerApplicationComponent
import javax.inject.Inject



/**
 * Created by Hospice on 20/03/2018.
 */

class MyApplication : Application() , HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
       return  activityDispatchingAndroidInjector
    }


    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder()
                .application(this)
                .build()
                .inject(this)


    }

}

