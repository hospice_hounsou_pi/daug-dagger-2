package daug.dependencies.injections.dinewway.module

import dagger.Module
import dagger.Provides
import daug.dependencies.injections.dinewway.ui.MainActivityContrat
import daug.dependencies.injections.dinewway.ui.MainPresenter


/**
 * Created by Hospice on 20/03/2018.
 */

@Module
class MainActivityModule {

    @Provides
    fun provideMainPresenter(presenter: MainPresenter<MainActivityContrat.View>): MainActivityContrat.Presenter<MainActivityContrat.View> {
        return presenter
    }

}
