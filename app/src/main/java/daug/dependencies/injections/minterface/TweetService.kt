package daug.dependencies.injections.minterface

import android.util.Log

/**
 * Created by Hospice on 20/03/2018.
 */
class TweetService : MessageService {
    override fun sendMessage(subject: String, message: String) {
        Log.e(TweetService::class.java.name, "Tweet: $subject, $message")
    }
}
