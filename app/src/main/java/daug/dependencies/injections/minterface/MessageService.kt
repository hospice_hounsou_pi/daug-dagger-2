package daug.dependencies.injections.minterface

/**
 * Created by Hospice on 20/03/2018.
 */
interface MessageService {
    fun sendMessage(subject: String, message: String)
}