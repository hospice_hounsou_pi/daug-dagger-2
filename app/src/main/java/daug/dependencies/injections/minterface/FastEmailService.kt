package daug.dependencies.injections.minterface

import android.util.Log

/**
 * Created by Hospice on 20/03/2018.
 */
class FastEmailService : MessageService {
    override fun sendMessage(subject: String, message: String) {
        Log.e(FastEmailService::class.java.name, "Fast Email: $subject, $message")
    }
}
